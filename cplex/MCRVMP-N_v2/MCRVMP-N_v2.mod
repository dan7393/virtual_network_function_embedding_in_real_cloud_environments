/*********************************************
 * OPL 12.6.3.0 Model
 * Author: Daniel
 * Creation Date: 12-12-2016 at 14:22:15
 *********************************************/
int numberOfHosts =...;
int numberOfVMs = ...;
int numberOfCuts =...;

range HOSTS = 1..numberOfHosts;
range VMs = 1..numberOfVMs;
range CUTS = 1..numberOfCuts;
range DCUTS = numberOfCuts+1..2*numberOfCuts; // ???
range ACUTS = 1..2*numberOfCuts; // ???

float CPU_req[VMs] =...;
float MEM_req[VMs] =...;
float NET_req[VMs] =...;
float CPU_cap[HOSTS] =...;
float MEM_cap[HOSTS] =...;
float NET_cap[HOSTS] =...;
float T[VMs][VMs] = ...;	// ruch miedzy VMkami
int C[ACUTS][HOSTS] = ...;	//obecno�� danego hosta w danym wycinku sieci

int ones[i in ACUTS][j in HOSTS] = 1;	// tablica taka jak C, tylko, ze z samymi 1
int C1[i in ACUTS][j in HOSTS] = ones[i][j] - C[i][j];	// 1-C
int Ct[j in HOSTS][i in ACUTS] = C[i][j];	//transpozycja macierzy C
int C1t[j in HOSTS][i in ACUTS] = C1[i][j];	//transpozycja macierzy (1-C)

dvar boolean X[VMs][HOSTS];	//zmienna decyzyjna, rozmieszczenie VMek na hostach
dvar boolean Xt[h in HOSTS][v in VMs] = X[v][h]; //transpozycja macierzy X
dvar float+ CLR[ACUTS];	// do 0 - 1 , float

// (5)
dvar float+ C_Xt[CUTS][VMs]; // 1
dvar float+ C_Xt_T[CUTS][VMs]; // 2
dvar float+ C_Xt_T_X[CUTS][HOSTS]; // 3
dvar float+ C_Xt_T_X_C1t[CUTS][CUTS]; // 4

// (6)
dvar float+ C1_Xt[DCUTS][VMs]; // 1
dvar float+ C1_Xt_T[DCUTS][VMs]; // 2
dvar float+ C1_Xt_T_X[DCUTS][HOSTS]; // 3
dvar float+ C1_Xt_T_X_Ct[DCUTS][DCUTS]; // 4

minimize max (p in ACUTS) CLR[p];
	
subject to{
	// (2)
	forall (j in HOSTS) sum(i in VMs) CPU_req[i]*X[i][j]<=CPU_cap[j];
	// (3)
	forall (j in HOSTS) sum(i in VMs) MEM_req[i]*X[i][j]<=MEM_cap[j];
	// (4)
	forall (j in HOSTS) sum(i in VMs) NET_req[i]*X[i][j]<=NET_cap[j];
	// (5)
	forall(i in CUTS, j in VMs) C_Xt[i][j] == sum(h in HOSTS) C[i][h]*Xt[h][j]; // C*Xt
	forall(i in CUTS, j in VMs) C_Xt_T[i][j] == sum(v in VMs) C_Xt[i][v]*T[v][j]; // C*Xt*T
	forall(i in CUTS, j in HOSTS) C_Xt_T_X[i][j] == sum(v in VMs) C_Xt_T[i][v]*X[v][j]; // C*Xt*T
	forall(i in CUTS, j in CUTS) C_Xt_T_X_C1t[i][j] == sum(h in HOSTS) C_Xt_T_X[i][h]*C1t[h][j]; // C*Xt*T
	forall(i in CUTS) CLR[i] == C_Xt_T_X_C1t[i][i];
	// (6)
	forall(i in DCUTS, j in VMs) C1_Xt[i][j] == sum(h in HOSTS) C1[i][h]*Xt[h][j]; // 
	forall(i in DCUTS, j in VMs) C1_Xt_T[i][j] == sum(v in VMs) C1_Xt[i][v]*T[v][j]; // 
	forall(i in DCUTS, j in HOSTS) C1_Xt_T_X[i][j] == sum(v in VMs) C1_Xt_T[i][v]*X[v][j]; // 
	forall(i in DCUTS, j in DCUTS) C1_Xt_T_X_Ct[i][j] == sum(h in HOSTS) C1_Xt_T_X[i][h]*Ct[h][j]; // 
	forall(i in DCUTS) CLR[i] == C1_Xt_T_X_Ct[i][i];
	// (7)
	forall(c in ACUTS) CLR[c] <= 1;
	// (8)
	forall(i in VMs) (sum(j in HOSTS) X[i][j]) == 1; 
	
	
	
	
}

//zmienne dodatnie
//nazwac grupy ograniczen - powie wtedy w ktorym ograniczeniu
//mozna wywalic zmienne pomocnicze transponowanie tylko sie odwolywac do odwrotnych indeksów
//


