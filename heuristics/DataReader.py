import VM
import Host

import numpy as np
import ast

def non_blank_lines(file):
    for line in file:
        if line.rstrip():
            yield line.rstrip()


class DataReader(object):
    def readDatFile(self, file):
        lines_list = []
        with open(file, 'r') as f:
            for line in non_blank_lines(f):
                lines_list.append(line)
        return lines_list

    def prepare_string_dict(self, lines_list):
        result_string_dict = {}

        result_string_dict["T"] = ""
        result_string_dict["C"] = ""

        counter = 0
        tmp_counter = 1
        for line in lines_list:
            if counter <= 8:
                (key,value)=(line.rstrip("; ]").replace("[", "").split(" = "))
                result_string_dict[key] = value
            # elif 9 <= counter <= 10 + int(result_string_dict["numberOfVMs"]):
            #     result_string_dict["T"] = str(result_string_dict["T"]) + str(line.rstrip("; ]").replace("[", "").split(" = "))
            #     tmp_counter += 1
            # elif 11 + tmp_counter <= counter:
            #     result_string_dict["C"] = str(result_string_dict["C"]) + str(line.rstrip("; ]").replace("[", "").split(" = "))
            elif 9 == counter:
                result_string_dict["T"] = str(result_string_dict["T"]) + str(
                    line.rstrip("; ]").replace("[", "").split(" = "))
                tmp_counter += 1
            else:
                result_string_dict["C"] = str(result_string_dict["C"]) + str(
                    line.rstrip("; ]").replace("[", "").split(" = "))

            counter += 1

        result_string_dict["CPU_req"] = result_string_dict["CPU_req"].split()
        result_string_dict["MEM_req"] = result_string_dict["MEM_req"].split()
        result_string_dict["NET_req"] = result_string_dict["NET_req"].split()

        result_string_dict["CPU_cap"] = result_string_dict["CPU_cap"].split()
        result_string_dict["MEM_cap"] = result_string_dict["MEM_cap"].split()
        result_string_dict["NET_cap"] = result_string_dict["NET_cap"].split()

        result_string_dict["T"] = result_string_dict["T"].replace("T", "").replace("[", "").replace("]", "").replace("'", "").replace(",", "").split()
        result_string_dict["C"] = result_string_dict["C"].replace("C", "").replace("[", "").replace("]", "").replace("'", "").replace(",", "").split()

        return result_string_dict

    def prepare_results(self, string_dict):
        results = {}

        results["numberOfHosts"] = int(string_dict["numberOfHosts"])
        results["numberOfVMs"] = int(string_dict["numberOfVMs"])
        results["numberOfCuts"] = int(string_dict["numberOfCuts"])

        results["CPU_req"] = [float(x) for x in string_dict["CPU_req"]]
        results["MEM_req"] = [float(x) for x in string_dict["MEM_req"]]
        results["NET_req"] = [float(x) for x in string_dict["NET_req"]]

        results["CPU_cap"] = [float(x) for x in string_dict["CPU_cap"]]
        results["MEM_cap"] = [float(x) for x in string_dict["MEM_cap"]]
        results["NET_cap"] = [float(x) for x in string_dict["NET_cap"]]

        T = []
        number_of_VMs = results["numberOfVMs"]
        for i in range( number_of_VMs ):
            T.append([float(x) for x in string_dict["T"][i*number_of_VMs:(i+1)*number_of_VMs ]])

        C = []
        number_of_hosts = results["numberOfHosts"]
        number_of_cuts = results["numberOfCuts"]
        for i in range( number_of_cuts ):
            C.append([int(x) for x in string_dict["C"][i*number_of_hosts:(i+1)*number_of_hosts ]])

        results["T"] = np.matrix(T)
        results["C"] = np.matrix(C)

        return results

    def get_data(self, file):
        lines_list = self.readDatFile(file)
        result_string_dict=self.prepare_string_dict(lines_list)
        data = self.prepare_results(result_string_dict)
        return data

if __name__ == "__main__":
    # file = "..\matlab\input_data.dat"
    file = "..\matlab\input_data_2H_2V_1.dat"
    data_reader = DataReader()
    data = data_reader.get_data(file)

    for d in data:
        print(d)
        print(data[d])
        print()


