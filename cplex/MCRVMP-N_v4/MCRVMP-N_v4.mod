/*********************************************
 * OPL 12.6.0.0 Model
 * Author: Administrator
 * Creation Date: 04-01-2017 at 18:46:39
 *********************************************/
 
int numberOfHosts =...;
int numberOfVMs = ...;
int numberOfCuts =...; // to zwracane jest jako ACUTS
int tvm=numberOfVMs*numberOfVMs;
int halfNumberOfCuts = ftoi(numberOfCuts/2); 
range HOSTS = 1..numberOfHosts;
range VMs = 1..numberOfVMs;
range CUTS = 1..halfNumberOfCuts;
range DCUTS = (halfNumberOfCuts+1)..numberOfCuts; // ???
range ACUTS = 1..numberOfCuts; // ???
range TVMs = 1..tvm;

float CPU_req[VMs] =...;
float MEM_req[VMs] =...;
float NET_req[VMs] =...;
float CPU_cap[HOSTS] =...;
float MEM_cap[HOSTS] =...;
float NET_cap[HOSTS] =...;
float T[TVMs] = ...;	// ruch miedzy VMkami// teraz jako wektor
int C[ACUTS][HOSTS] = ...;	//obecno danego hosta w danym wycinku sieci

int ones[i in ACUTS][j in HOSTS] = 1;	// tablica taka jak C, tylko, ze z samymi 1
int C1[i in ACUTS][j in HOSTS] = ones[i][j] - C[i][j];	// 1-C
int Ct[j in HOSTS][i in ACUTS] = C[i][j];	//transpozycja macierzy C
int C1t[j in HOSTS][i in ACUTS] = C1[i][j];	//transpozycja macierzy (1-C)

dvar boolean X[VMs][HOSTS];	//zmienna decyzyjna, rozmieszczenie VMek na hostach
dvar float+ CLR[ACUTS];	// do 0 - 1 , float

dvar boolean Z[HOSTS][HOSTS][TVMs]; // Xt*T*X -> na dodawanie
dvar float+ ZT[HOSTS][HOSTS];
// (5)
dvar float+ C_ZT[CUTS][HOSTS]; // 3
dvar float+ C_ZT_C1t[CUTS][CUTS]; // 4

// (6)
dvar float+ C1_ZT[DCUTS][HOSTS]; //3
dvar float+ C1_ZT_Ct[DCUTS][DCUTS]; // 4


minimize max (p in ACUTS) CLR[p];
						
subject to{
	Dwa: // (2)
		forall (j in HOSTS) sum(i in VMs) CPU_req[i]*X[i][j]<=CPU_cap[j];
	Trzy:// (3)
		forall (j in HOSTS) sum(i in VMs) MEM_req[i]*X[i][j]<=MEM_cap[j];
		
	MatrixZ:
	
	forall(hw in HOSTS){//wiersze macierzy z
		forall(hk in HOSTS){//kolumny w z
				forall(vmt in TVMs){
						Z[hw][hk][vmt] <= X[ftoi(round(vmt/numberOfVMs+0.49))][hw]; //x transponowane
					
						if(vmt%numberOfVMs!=0){
						Z[hw][hk][vmt] <= X[vmt%numberOfVMs][hk]; //x normalne
						Z[hw][hk][vmt] >= X[ftoi(round(vmt/numberOfVMs+0.49))][hw] + X[vmt%numberOfVMs][hk] - 1;
    					}
    					else{
    					Z[hw][hk][vmt] <= X[numberOfVMs][hk]; //x normalne    					
    					Z[hw][hk][vmt] >= X[ftoi(round(vmt/numberOfVMs+0.49))][hw] + X[numberOfVMs][hk] - 1; 
    					}						
}
}
}			
	//wymnozenie z przez T
    forall(hw in HOSTS){//wiersze macierzy z
    	forall(hk in HOSTS){//kolumny w z
		 		ZT[hw][hk] == sum(vm in TVMs) Z[hw][hk][vm]*T[vm]; //to juz powinna byc macierz zawierajaca wyniki Xt*T*X
        }
    }	
    
	Piec:// (4)
	forall(i in CUTS, j in HOSTS)  C_ZT[i][j] == sum(h in HOSTS) C[i][h]*ZT[h][j]; // C*ZT
	forall(i in CUTS, j in CUTS) C_ZT_C1t[i][j] == sum(h in HOSTS) C_ZT[i][h]*C1t[h][j]; // C*ZT*C1T
	forall(i in CUTS) CLR[i] == C_ZT_C1t[i][i]/1000;
	
	Szesc:// (5)
	forall(i in DCUTS, j in HOSTS) C1_ZT[i][j] == sum(h in HOSTS) C1[i][h]*ZT[h][j]; //
	forall(i in DCUTS, j in DCUTS) C1_ZT_Ct[i][j] == sum(h in HOSTS) C1_ZT[i][h]*Ct[h][j]; // 
	forall(i in DCUTS) CLR[i] == C1_ZT_Ct[i][i]/1000;
	
	siedem:// (7)
		forall(c in ACUTS) CLR[c] <= 1;
	osiem:// (8)
		forall(i in VMs) (sum(j in HOSTS) X[i][j]) == 1; 

}

execute{

write("Wartosc optymalna CLR wynosi:",cplex.getObjValue());

}
 