/*********************************************
 * OPL 12.6.3.0 Model
 * Author: Daniel
 * Creation Date: 12-12-2016 at 14:22:15
 *********************************************/
int numberOfHosts =...;
int numberOfVMs = ...;
int numberOfCuts =...; // to zwracane jest jako ACUTS
int halfNumberOfCuts = ftoi(numberOfCuts/2); 
range HOSTS = 1..numberOfHosts;
range VMs = 1..numberOfVMs;
range CUTS = 1..halfNumberOfCuts;
range DCUTS = (halfNumberOfCuts+1)..numberOfCuts; // ???
range ACUTS = 1..numberOfCuts; // ???

float CPU_req[VMs] =...;
float MEM_req[VMs] =...;
float NET_req[VMs] =...;
float CPU_cap[HOSTS] =...;
float MEM_cap[HOSTS] =...;
float NET_cap[HOSTS] =...;
float T[VMs][VMs] = ...;	// ruch miedzy VMkami
float Tinv[VMs][VMs] = ...;	// ruch miedzy VMkami
int C[ACUTS][HOSTS] = ...;	//obecno�� danego hosta w danym wycinku sieci

int ones[i in ACUTS][j in HOSTS] = 1;	// tablica taka jak C, tylko, ze z samymi 1
int C1[i in ACUTS][j in HOSTS] = ones[i][j] - C[i][j];	// 1-C

dvar boolean X[VMs][HOSTS];	//zmienna decyzyjna, rozmieszczenie VMek na hostach
dvar float+ CLR[ACUTS];	// do 0 - 1 , float

dvar float+ Z_tmp[HOSTS][HOSTS]; // Xt*X -> na dodawanie
dvar float+ Z[HOSTS][HOSTS]; // Z*T -> element razy element, nie macierzowo
// (5)
dvar float+ C_Xt_T_X_C1t[CUTS][CUTS]; // 4
// (6)
dvar float+ C1_Xt_T_X_Ct[DCUTS][DCUTS]; // 4

minimize max (p in ACUTS) CLR[p];
	
subject to{
	Dwa: // (2)
		forall (j in HOSTS) sum(i in VMs) CPU_req[i]*X[i][j]<=CPU_cap[j];
	Trzy:// (3)
		forall (j in HOSTS) sum(i in VMs) MEM_req[i]*X[i][j]<=MEM_cap[j];
	Cztery:// (4)
		forall (j in HOSTS) sum(i in VMs) NET_req[i]*X[i][j]<=NET_cap[j];
	
//		forall(i in DCUTS) CLR[i] == C1_Xt_T_X_Ct[i][i];
//	zamiana_na_dodawanie: // zastepujemy tylko mnozenie zmiennych na dodawanie
//		forall(h1 in HOSTS, h2 in HOSTS){		
//			Z_tmp[h1][h2] == sum(v1 in VMs, v2 in VMs) (X[v1][h1]+ X[v2][h2]) ; // Xt*X z odpowiednie elementy		
//		}
	zamiana_na_dodawanie2: // HOSTSxHOSTS, wszystkie mozliwe kombinacje mnozenia zmiennych 
		forall(h1 in HOSTS, h2 in HOSTS, v1 in VMs, v2 in VMs){ // to
			Z_tmp[h1][h2] <= X[v1][h1]; // Xt
			Z_tmp[h1][h2] <= X[v2][h2]; // X
			Z_tmp[h1][h2] >= X[v1][h1] + X[v2][h2] - 1;			
		}
		
//	zamiana_na_dodawanie3: // zastepujemy tylko mnozenie zmiennych na dodawanie
//		forall(h1 in HOSTS, h2 in HOSTS){		
//			Z[h1][h2] == sum(v in VMs) (Tinv[h1][v] * Z_tmp[v][h2]); 
//		}
	
	Piec:// (5)
		forall(i in CUTS, j in CUTS) {// C*Xt*T*X*(1-C)t
			C_Xt_T_X_C1t[i][j] == sum(d in HOSTS) sum(c in VMs) sum(b in VMs) sum(a in HOSTS)  C[i][a]*Tinv[]*Z[a][d]*C1[j][d];
		}
		forall(i in CUTS) CLR[i] == C_Xt_T_X_C1t[i][i];
	szesc:// (6)
		forall(i in DCUTS, j in DCUTS){// (1-C)*Xt*T*X*Ct
			C1_Xt_T_X_Ct[i][j] == sum(d in HOSTS) sum(c in VMs) sum(b in VMs) sum(a in HOSTS) C1[i][a]*Z[a][d]*C[j][d]; //
		}  
		forall(i in DCUTS) CLR[i] == C1_Xt_T_X_Ct[i][i];
	
	siedem:// (7)
		forall(c in ACUTS) CLR[c] <= 1;
	osiem:// (8)
		forall(i in VMs) (sum(j in HOSTS) X[i][j]) == 1; 
	
	
	
	
}


