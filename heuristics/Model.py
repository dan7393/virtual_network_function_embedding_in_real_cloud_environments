from DataReader import DataReader
from VM import VM
from Host import Host

from operator import itemgetter
import random
import numpy as np
import time

class Model(object):
    def __init__(self, file):
        self.file = file

    def prepare_x_matrix(self):
        data_reader= DataReader()
        data = data_reader.get_data(self.file)
        self.data = data

        number_of_hosts = data["numberOfHosts"]
        number_of_vms = data["numberOfVMs"]

        self.hosts = [Host(data["CPU_cap"][x],data["MEM_cap"][x],data["NET_cap"][x]) for x in range(number_of_hosts)]
        self.vms = [VM(data["CPU_req"][x],data["MEM_req"][x],data["NET_req"][x], x) for x in range(number_of_vms)]

        self.hosts_choices = [x for x in range(number_of_hosts)]
        self.vms_choices = [x for x in range(number_of_vms)]

        counter = 0
        counter2 = 0
        while len(self.vms_choices) != 0:
            counter += 1
            selected_host = random.choice(self.hosts_choices)
            selected_vm = random.choice(self.vms_choices)

            if self.hosts[selected_host].add_VM(self.vms[selected_vm]):
                self.vms_choices.remove(selected_vm)
            if counter == 100 * number_of_vms * number_of_hosts:
                counter2 += 1

                self.hosts.clear()
                self.vms.clear()
                self.hosts = [Host(data["CPU_cap"][x], data["MEM_cap"][x], data["NET_cap"][x]) for x in range(number_of_hosts)]
                self.vms = [VM(data["CPU_req"][x], data["MEM_req"][x], data["NET_req"][x], x) for x in range(number_of_vms)]
                self.hosts_choices = [x for x in range(number_of_hosts)]
                self.vms_choices = [x for x in range(number_of_vms)]
                counter = 0
                if counter2 == 1000:
                    print("Brak rozwiazania")
                    break


        x = np.zeros((number_of_vms, number_of_hosts))
        for i in range (len(self.hosts)):
            for vm in range(len(self.hosts[i].VMs)):
                x.itemset((self.hosts[i].VMs[vm].seq_number,i),1)
        self.X = x
        return x

    def calculate_CLR(self):
        CAP = 1000
        C = self.data["C"]
        T = self.data["T"]
        X = self.X

        ones = np.ones((self.data["numberOfCuts"], self.data["numberOfHosts"]))
        one_minus_c = ones - C

        CLR1 = (C*X.T*T*X*one_minus_c.T)/CAP
        CLR2 = (one_minus_c*X.T*T*X*C.T)/CAP

        max_clr1 = np.amax(CLR1)
        max_clr2 = np.amax(CLR2)

        if max_clr1 >= max_clr2:
            return max_clr1
        else:
            return max_clr2

    def optimize_CLR(self):
        stop = int(self.data["numberOfHosts"])
        initial_CLR = self.calculate_CLR()
        hosts_loads = self.calculate_hosts_loads()

        print(initial_CLR)

        number_of_iterations = 1
        counter = 0
        while counter < stop:
            hosts_loads = self.calculate_hosts_loads()
            vms = hosts_loads[counter]["Host"].get_sorted_vms()

            for vm in vms:
                number_of_iterations += 1

                if hosts_loads[len(hosts_loads) - 1]["Host"].add_VM(vm["VM"]):
                    hosts_loads[counter]["Host"].remove_VM(vm["VM"])

                    self.X.itemset((vm["VM"].seq_number, self.hosts.index(hosts_loads[counter]["Host"])), 0)
                    self.X.itemset((vm["VM"].seq_number, self.hosts.index(hosts_loads[len(hosts_loads) - 1]["Host"])), 1)

                    print(self.calculate_CLR())

                    if initial_CLR >= self.calculate_CLR():
                        initial_CLR = self.calculate_CLR()
                        break
                    else:
                        self.X.itemset((vm["VM"].seq_number, self.hosts.index(hosts_loads[counter]["Host"])), 1)
                        self.X.itemset((vm["VM"].seq_number, self.hosts.index(hosts_loads[len(hosts_loads) - 1]["Host"])), 0)

                        hosts_loads[len(hosts_loads) - 1]["Host"].remove_VM(vm["VM"])
                        hosts_loads[counter]["Host"].add_VM(vm["VM"])
                else:
                    print(self.calculate_CLR())

            counter += 1
            # print(self.calculate_CLR())

        print ("Number of iterations {0}".format(number_of_iterations))
        print("Final result {0}".format(self.calculate_CLR()))
    def calculate_hosts_loads(self):
        hosts_loads = [{"Host": host, "Load": sum([vm.CPU/host.CPU + vm.MEM/host.MEM + vm.NET/host.NET for vm in host.VMs])} for host in self.hosts]
        return sorted(hosts_loads, key=itemgetter("Load"), reverse=True)


if __name__ == "__main__":
    # file = "..\matlab\input_data_2H_2V_1.dat"
    # file = "..\matlab\input_data_2H_2V_2.dat"
    # file = "..\matlab\input_data_2H_3V_1.dat"
    # file = "..\matlab\input_data_2H_3V_2.dat"
    # file = "..\matlab\input_data_4H_4V_1.dat"
    # file = "..\matlab\input_data_4H_4V_2.dat"
    # file = "..\matlab\input_data_4H_8V_1.dat"
    # file = "..\matlab\input_data_4H_8V_2.dat"
    # file = "..\matlab\input_data_4H_10V_1.dat"
    # file = "..\matlab\input_data_4H_10V_2.dat"
    # file = "..\matlab\input_data_4H_16V_1.dat"
    # file = "..\matlab\input_data_4H_12V_1.dat"
    file = "..\matlab\input_data_4H_100V_1.dat"


    start_time = time.time()

    model = Model(file)
    model.prepare_x_matrix()
    result = model.optimize_CLR()

    end_time = time.time()

    print(end_time - start_time)
    # print()
    # for e in result:
    #     print (e)
    #     print()
