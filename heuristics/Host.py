from VM import VM

from operator import itemgetter

class Host(object):
    def __init__(self, CPU, MEM, NET):
        self.CPU = CPU
        self.MEM = MEM
        self.NET = NET
        self.VMs = []

    def CPU_sum(self):
        return sum([x.CPU for x in self.VMs])

    def MEM_sum(self):
        return sum([x.MEM for x in self.VMs])

    def NET_sum(self):
        return sum([x.NET for x in self.VMs])

    def add_VM(self, vm):
        if(vm.CPU + self.CPU_sum() > self.CPU):
            return False
        if(vm.MEM + self.MEM_sum() > self.MEM):
            return False
        if(vm.NET + self.NET_sum() > self.NET):
            return False
        self.VMs.append(vm)
        return True

    def remove_VM(self, vm):
        self.VMs.remove(vm)

    def get_sorted_vms(self):
        loads = [{"VM": vm, "Load": (vm.CPU/self.CPU + vm.MEM/self.MEM + vm.NET/self.NET)} for vm in self.VMs]
        return sorted(loads, key=itemgetter("Load"), reverse=True)

if __name__ == "__main__":
    host = Host(100,100,100)
    for i in range(10):
        host.add_VM(VM(10,11,12,1))

    print (host.CPU_sum())
    print (host.MEM_sum())
    print (host.NET_sum())

    a = host.get_sorted_vms()

    for e in a:
        host.remove_VM(e["VM"])
    print(len(host.VMs))