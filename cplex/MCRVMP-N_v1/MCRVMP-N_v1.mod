/*********************************************
 * OPL 12.6.3.0 Model
 * Author: Daniel
 * Creation Date: 21-11-2016 at 21:36:19
 *********************************************/
 // import variables from .dat file
 int n = ...;
 int m = ...;
 range H = 1..n;
 range VMs = 1..m;
 
 float CPUreq[VMs] = ...;
 float MEMreq[VMs] = ...;
 float NETreq[VMs] = ...;
 

 dvar float+ load;
 
 
 minimize 
 	load;
 	
 subject to{
 
   
 }