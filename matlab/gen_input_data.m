% Przedmiot : MPSiS
% Temat pracy: Virtual network function embedding in real cloud environments 
% 
% Skrypt w celu generowania danych wejsciowych 

numberOfHosts = 2; % jesli rozwazamy topologie drzewa, to musimy mie� wielokrotnosc potegi 2, 2^n
numberOfVMs = 20;

% sprawdza czy liczba hostow jest 2^n
if rem(log2(numberOfHosts), 1) > 0 
      numberOfHosts = 2^(round(log2(numberOfHosts))); % zaokragla w dol, do wielokrotnosci 2^n
end

% mozna wyprowadzic na podstawie drzewa, ze liczna butow jest zalezna od
% liczby hostow, bo dla :
% 2 hosts -> 2 cuts
% 4 hosts -> 6 cuts
% 8 hosts -> 14 cuts
% z tego wynika, ze jest to funkcja liniowa.
% po wyprowadzeniu jej dochodzimy do postaci : y = 2x - 2 , gdzie x to
% liczba hostow a y to liczba cutow
numberOfCuts = 2*numberOfHosts - 2 ;


% for VMs
CPU_req = rand(1, numberOfVMs) * 1.5 + 1.5; % VM min 1.5 max 3 GHz 
MEM_req = rand(1, numberOfVMs) * 7168 + 1024; % VM min 1024 max 8192 MB
NET_req = rand(1, numberOfVMs) * 100; % VM max 100 Mbit/s
% for Hosts
CPU_cap = rand(1, numberOfHosts)* 6 + 2; % VM min 2 max 8 GHz 
MEM_cap = rand(1, numberOfHosts)* 28672 + 4096; % VM min 4096 max 32768 MB
NET_cap = rand(1, numberOfHosts)* 1000 + 100; % VM max 1000 Mbit/s

% aby byl suma wszystkich VM by�a mniejsza lub r�wna zasobom host�w
repeat = 1;
while repeat == 1
      if sum(CPU_req) > sum(CPU_cap)*0.7
            CPU_cap = CPU_cap * 1.1;
      elseif sum(MEM_req) > sum(MEM_cap)*0.7
            MEM_cap = MEM_cap * 1.1;
      elseif sum(NET_req) > sum(NET_cap)*0.7
            NET_cap = NET_cap * 1.1; 
      else
            repeat = 0;
      end
end

sum_cpu_cap = sum(CPU_cap)
sum_cpu_req = sum(CPU_req)
sum_cpu_req/sum_cpu_cap

% Vector T:
% suma srednich ruchow jednej VM pomiedzy VMs musi byc rowna sumie wymaganej przepustowosci dla tej VM
% pseudo : sum(T(1,:)) + suma(T(:,1)) = NET_req(1);
% suma w pionie i w poziomie musi sie rownac wymaganej dla danej VM
% pasmo wyjsciowe i pasmo wejsciowe do VM = wymaganej przepustowosci
T = rand(numberOfVMs, numberOfVMs) .* ~eye(numberOfVMs,numberOfVMs)
zero_diagonal = ~eye(numberOfVMs,numberOfVMs);
zero_diagonal_duza = [zero_diagonal,zero_diagonal]; 

for i = 1:numberOfVMs
      tmp = rand(1, numberOfVMs*2); % musze wygenerowac randomowy ruch z kazdej vm do kazdej, oprocz do siebie
      tmp = tmp .* zero_diagonal_duza(i,:);
      tmp = tmp / sum(tmp); % normalizuje do sumy ich, aby suma calosci stanowila wymagana wartosc

      T(i,:) = tmp(1,1:numberOfVMs) * NET_req(i);
      T(:,i) = tmp(1,numberOfVMs+1:numberOfVMs*2)' * NET_req(i);
end

Ttmp = T';
Tvec = Ttmp(:)';

% Vector C:
% H1 - nie nalezy root
% H2 - nalezy root
% C[i,j) = 1, jesli hi nelazy do H1, gdzie d
C = ones(numberOfCuts, numberOfHosts);
C(1:numberOfHosts,:) = eye(numberOfHosts); % cuty na dolnej warstwie polaczen
cnt = 0;
for i = 1:log2(numberOfHosts)% ta petla mowi ile pieter cutow w drzewie
      for j = 1:numberOfHosts/(2^(i-1)) % ile razy musi sie powtorzyc w na danym pietrze
            C(cnt + j, :) = zeros(1,numberOfHosts);
            a = 2^(i-1)*j -(2^(i-1)-1);
            b = 2^(i-1)*j + (2^(i-1)-1) - (2^(i-1)-1);
            C(cnt + j, a : b) =  ones(1,2^(i-1));
      end
      cnt = cnt + numberOfHosts/(2^(i-1));
end

Ctmp = [C ; C];
C = [C ; C];
numberOfCuts = 2* numberOfCuts;

% Zapis do pliku

fid = fopen('input_data_2H_80V_1.dat','w');

fprintf(fid,'numberOfHosts = %d;\t\n', numberOfHosts);
fprintf(fid,'numberOfVMs = %d;\t\n', numberOfVMs);
fprintf(fid,'numberOfCuts = %d;\t\n', numberOfCuts);
fprintf(fid,'\t\n');

% Requirements
fprintf(fid,'CPU_req = [');
for i = 1:numberOfVMs
  fprintf(fid,' %.2f ', CPU_req(i));    
end
fprintf(fid,'];\t\n');
fprintf(fid,'MEM_req = [');
for i = 1:numberOfVMs
  fprintf(fid,' %.2f ', MEM_req(i));    
end
fprintf(fid,'];\t\n');
fprintf(fid,'NET_req = [');
for i = 1:numberOfVMs
  fprintf(fid,' %.2f ', NET_req(i));    
end
fprintf(fid,'];\t\n');

fprintf(fid,'\t\n');

% Capacity
fprintf(fid,'CPU_cap = [');
for i = 1:numberOfHosts
  fprintf(fid,' %.2f ', CPU_cap(i));    
end
fprintf(fid,'];\t\n');
fprintf(fid,'MEM_cap = [');
for i = 1:numberOfHosts
  fprintf(fid,' %.2f ', MEM_cap(i));    
end
fprintf(fid,'];\t\n');
fprintf(fid,'NET_cap = [');
for i = 1:numberOfHosts
  fprintf(fid,' %.2f ', NET_cap(i));    
end
fprintf(fid,'];\t\n');

fprintf(fid,'\t\n');

fprintf(fid,'T = [');
for i = 1:(numberOfVMs^2)
   fprintf(fid,' %.2f ', Tvec(i));       
end
fprintf(fid,'];\t\n');

% fprintf(fid,'T = [\t\n');
% for i = 1:numberOfVMs
%       fprintf(fid,'[');
%       for j = 1:numberOfVMs
%             fprintf(fid,' %.2f ', T(i,j));    
%       end
%       fprintf(fid,']\t\n');   
% end
% fprintf(fid,'];\t\n');

% fprintf(fid,'Tinv = [\t\n');
% for i = 1:numberOfVMs
%       fprintf(fid,'[');
%       for j = 1:numberOfVMs
%             fprintf(fid,' %.2f ', Tinv(i,j));    
%       end
%       fprintf(fid,']\t\n');   
% end
% fprintf(fid,'];\t\n');
% 
% fprintf(fid,'\t\n');

fprintf(fid,'C = [\t\n');
for i = 1:numberOfCuts
      fprintf(fid,'[');
      for j = 1:numberOfHosts
            fprintf(fid,' %.0f ', round(C(i,j)));    
      end
      fprintf(fid,']\t\n');   
end
fprintf(fid,'];\t\n');


fclose(fid);





